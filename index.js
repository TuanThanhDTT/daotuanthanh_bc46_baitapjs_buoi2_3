/**
 * Bài 1
 */
function tinhLuong(){
    var luongCoBan = document.getElementById("luongCoBan").value*1;
    var soNgayLam = document.getElementById("soNgayLam").value*1;
    var tongLuong = (luongCoBan * soNgayLam);
    document.getElementById("tongLuong").innerHTML =`Tổng lương được nhận là: ${tongLuong}vnd`;
};

/**
 * Bài 2
 */
function tinhTrungBinhCong(){
    var num1 = document.getElementById("num1").value*1;
    var num2 = document.getElementById("num2").value*1;
    var num3 = document.getElementById("num3").value*1;
    var num4 = document.getElementById("num4").value*1;
    var num5 = document.getElementById("num5").value*1;
    var trungBinhCong = (num1 + num2 + num3 + num4 + num5) / 5;
    document.getElementById("trungBinhCong").innerHTML =`Trung bình cộng là: ${trungBinhCong}`;
};
/**
 * Bài 3
 */
function doiTien(){
    var USD = document.getElementById("USD").value*1;
    var VND = (USD * 23500.00);
    document.getElementById("money").innerHTML = `Số tiền sau khi chuyển đổi là: ${VND}vnđ`;
};
/**
 * Bài 4
 */
function tinhCvDt(){
    var chieuDai = document.getElementById("chieuDai").value*1;
    var chieuRong = document.getElementById("chieuRong").value*1;
    var chuVi = (chieuDai + chieuRong) / 2;
    var dienTich = (chieuDai * chieuRong);
    document.getElementById("ketqua1").innerHTML = `Chu vi là: ${chuVi}`;
    document.getElementById("ketqua2").innerHTML = `Diện tích là: ${dienTich}`;
};
/**
 * Bài 5
 */
function tinhTong(){
    var num = document.getElementById("num").value*1;
    var numDonVi = num % 10;
    var numHangChuc = (num - numDonVi) / 10;
    var tongHaiKySo = numDonVi + numHangChuc;
    document.getElementById("sum").innerHTML = `Tổng hai ký số là: ${tongHaiKySo}`;
};
